var searchData=
[
  ['mail_5fcheck_5fmask_5fcod_5fpayment',['MAIL_CHECK_MASK_COD_PAYMENT',['../group__mailing.html#gga0409511b5ca7192d457a7e550a1c6e26a4cef0afe6ff002d8e7089cb2eddf1468',1,'Mail.h']]],
  ['mail_5fcheck_5fmask_5fcopied',['MAIL_CHECK_MASK_COPIED',['../group__mailing.html#gga0409511b5ca7192d457a7e550a1c6e26a9170ec2a04d365ca0a6167cad4a2e2fb',1,'Mail.h']]],
  ['mail_5fcheck_5fmask_5fhas_5fbody',['MAIL_CHECK_MASK_HAS_BODY',['../group__mailing.html#gga0409511b5ca7192d457a7e550a1c6e26a348c2227a2767173436f2f88dfe37716',1,'Mail.h']]],
  ['mail_5fcheck_5fmask_5fread',['MAIL_CHECK_MASK_READ',['../group__mailing.html#gga0409511b5ca7192d457a7e550a1c6e26a3b48ed5fecdcd88c0a15e74b0853c03b',1,'Mail.h']]],
  ['mail_5fcheck_5fmask_5freturned',['MAIL_CHECK_MASK_RETURNED',['../group__mailing.html#gga0409511b5ca7192d457a7e550a1c6e26a5f7f5d671d1137cc0dd8b88a6579454a',1,'Mail.h']]],
  ['mail_5fgameobject',['MAIL_GAMEOBJECT',['../group__mailing.html#ggafb83216fd8c410f5d4bdfef0f9b302eda4398082ad7546f3811f818835522100d',1,'Mail.h']]],
  ['mail_5fitem',['MAIL_ITEM',['../group__mailing.html#ggafb83216fd8c410f5d4bdfef0f9b302eda544753dcb8561fd6e8aaba18cdbcf0a9',1,'Mail.h']]]
];
