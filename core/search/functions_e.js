var searchData=
[
  ['name',['name',['../classG3D_1_1Any.html#a6a22188c1122e6a02565fb03449b33da',1,'G3D::Any::name()'],['../classG3D_1_1GThread.html#a8ccb44cf99a0857dca30e99ba21bac06',1,'G3D::GThread::name()'],['../classG3D_1_1ImageFormat.html#aaee80a8c789c01f0ae23f37d145bcc21',1,'G3D::ImageFormat::name()']]],
  ['nan',['nan',['../g3dmath_8h.html#ad12b2c2b8d209674d34c141b4b430f93',1,'G3D']]],
  ['nanometers',['nanometers',['../namespaceG3D_1_1units.html#a49bbea930ccdd683a8e8b7a9d5cb2f00',1,'G3D::units']]],
  ['nanoseconds',['nanoseconds',['../namespaceG3D_1_1units.html#a7fae5d7f2439606332b13babf2c4a650',1,'G3D::units']]],
  ['nearest',['nearest',['../classG3D_1_1Map2D.html#afce29afd990b9b709da208c0ad24e5de',1,'G3D::Map2D']]],
  ['nearplanez',['nearPlaneZ',['../classG3D_1_1GCamera.html#a44965e5f86d974a0438525603a4afe7a',1,'G3D::GCamera']]],
  ['netaddress',['NetAddress',['../classG3D_1_1NetAddress.html#aac3810edb19320c6ce60ca3999f51a76',1,'G3D::NetAddress::NetAddress(uint32 host, uint16 port=0)'],['../classG3D_1_1NetAddress.html#a38d14dd36acb8ce086d9a93b8e10d295',1,'G3D::NetAddress::NetAddress(const std::string &amp;hostname, uint16 port)'],['../classG3D_1_1NetAddress.html#ae457f90f181a50d48b4d2fed1f46828c',1,'G3D::NetAddress::NetAddress(const std::string &amp;hostnameAndPort)']]],
  ['next',['next',['../classG3D_1_1Any.html#af85ce9152cb242532bedae5d3ecc38a4',1,'G3D::Any::next()'],['../classG3D_1_1Array.html#ae8d77536699b572af1e3b442f718f6f3',1,'G3D::Array::next()'],['../classACE__Based_1_1LockedQueue.html#a8ea375bcfb11a6f58fa324c26564017c',1,'ACE_Based::LockedQueue::next()']]],
  ['nlerp',['nlerp',['../classG3D_1_1Quat.html#ac0d6f4d5c0cb4d6d225f2569f02a2dd1',1,'G3D::Quat']]],
  ['node',['Node',['../classG3D_1_1KDTree_1_1Node.html#abaa4e778be374a73104fb755f535c323',1,'G3D::KDTree::Node::Node()'],['../classG3D_1_1KDTree_1_1Node.html#a7c0d97a38112b014af3f64573daa50f8',1,'G3D::KDTree::Node::Node(const Node &amp;other)'],['../classG3D_1_1KDTree_1_1Node.html#a4988f7e7ec624c4f20d7d9bea2b6df7f',1,'G3D::KDTree::Node::Node(const Array&lt; Handle *&gt; &amp;pt)'],['../classG3D_1_1PointKDTree_1_1Node.html#a4449621d7b2c2d4fff24ba83e24854aa',1,'G3D::PointKDTree::Node::Node()'],['../classG3D_1_1PointKDTree_1_1Node.html#ad26ed35af8e72bf0aeadd65b1866fa37',1,'G3D::PointKDTree::Node::Node(const Node &amp;other)'],['../classG3D_1_1PointKDTree_1_1Node.html#abe1aaf443139d488a1cb60957432a290',1,'G3D::PointKDTree::Node::Node(const Array&lt; Handle &gt; &amp;pt)']]],
  ['norm',['norm',['../classG3D_1_1Matrix.html#a55a5708512effe359ab294bfe61dfcfc',1,'G3D::Matrix::norm()'],['../classG3D_1_1Quat.html#adcf6dcfb14f4b8f6d035ca3264e43246',1,'G3D::Quat::norm()']]],
  ['normalize',['normalize',['../g3dmath_8h.html#a1fa53333af2cacda5b7aebc1b9645343',1,'G3D']]],
  ['normsquared',['normSquared',['../classG3D_1_1Matrix.html#ac0a12fa411eaae2e589cfd739ef0c228',1,'G3D::Matrix']]],
  ['number',['number',['../classG3D_1_1Any.html#ac6b1001ba5b012e21f774b3a3278d125',1,'G3D::Any::number()'],['../classG3D_1_1AnyVal.html#ac78ab7575fc8feb08a6fd2b58473b871',1,'G3D::AnyVal::number() const'],['../classG3D_1_1AnyVal.html#a8333fecec3ecdbf2636d7fa05eef6004',1,'G3D::AnyVal::number(double defaultVal) const'],['../classG3D_1_1Token.html#a6c95e81bc74f710bd769d69df5dddc5d',1,'G3D::Token::number()']]],
  ['numcores',['numCores',['../classG3D_1_1System.html#a4acba57a7c4353b59622dd6225f8e509',1,'G3D::System']]],
  ['numsmallprimes',['numSmallPrimes',['../classG3D_1_1Crypto.html#a707d0c2746bf9656f44da0b576930984',1,'G3D::Crypto']]],
  ['numstarted',['numStarted',['../classG3D_1_1ThreadSet.html#a6ba51168027429cd0a06b8339c2982bf',1,'G3D::ThreadSet']]],
  ['numvertices',['numVertices',['../classG3D_1_1ConvexPolygon.html#a4d188f3913bca737bf6e9cfe21414c52',1,'G3D::ConvexPolygon']]]
];
